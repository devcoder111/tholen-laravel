<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// To get the all floors
Route::get(
    '/floors',
    function (Request $request) {
        return \App\Floor::all();
    }
);

// To get the specific floor
Route::get(
    '/floor/{id}',
    function ($id) {
        return \App\Floor::find($id);
    }
);

// To get the last floor
Route::get(
    '/last-floor/',
    function () {
        return \App\Floor::orderBy('id', 'desc')->first();
    }
);

// get the layers.
Route::get(
    '/layers',
    function () {
        return \App\floorLayers::all();
    }
);



//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
