
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel='stylesheet' id='fontawesome-css' href='https://use.fontawesome.com/releases/v5.0.1/css/all.css?ver=4.9.1' type='text/css' media='all' />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link href="https://fonts.googleapis.com/css?family=Sintony:400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet " href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet " href="{{asset('assets/css/bootstrap-select.min.css')}}">

    <title>Dashboard - Thoolen's</title>
</head>
<body>

<div class="navigation add-floorplan">
    <div class="col-12 config-aside">
        <ul class="nav nav-tabs comon-tabs" id="configTabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="display-tab-link" data-toggle="tab" href="#display_tab" role="tab" aria-controls="home" aria-selected="true">Office Floorplan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="spec-tab-link" data-toggle="tab" href="#spec_tab" role="tab" aria-controls="profile" aria-selected="false">Specifications</a>
            </li>
           
        </ul>

        @if($errors->all())
            <br>
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                @endforeach
            </div>
        @endif
        @if(session()->has('success'))
            <br>
            <div class="alert alert-success">
               {{session()->get('success')}}!
            </div>
        @endif
        <form action="{{url('floor_plan')}}" class="form" method="post">
            {{csrf_field()}}
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="display_tab" role="tabpanel" aria-labelledby="display-tab-link">
                    <div class="row gutter-10">
                        <div class="col-md-6 col-6 form-group">
                            <label for="Operatory Rooms">Number of Operatory Rooms</label>
                            <div class="quantity">
                                <input type="number" id="rooms" name="operatory_rooms" min="1" max="100" step="1" value="1" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-6 form-group">
                            <label for="Square Footage">Total Office Square Footage</label>
                            <div class="quantity">
                                <input type="number" id="footage" min="450" name="square_footage" value="500" required>
                            </div>
                        </div>
                    </div>

                    <div class="row gutter-10 form-group mb-sm">
                        <div class="col-12">
                            <label for="application">Select Building Shape <span>(with North side facing up)</span></label>
                            <div class="wrap-shape">
                                <div class="shape-1">
                                    <input type="radio" name="building_shape" value="70">
                                </div>
                                <div class="shape-2">
                                    <input type="radio" name="building_shape" value="60">
                                </div>
                                <div class="shape-3">
                                    <input type="radio" name="building_shape" value="50">
                                </div>
                                <div class="shape-4">
                                    <input type="radio" name="building_shape" value="40">
                                </div>
                                <div class="shape-5">
                                    <input type="radio" name="building_shape" value="30">
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row gutter-10">
                        <div class="col-md-6 col-6 form-group">
                            <label for="display_make">Frontdoor Facing:</label>
                            <select name="frontdoor_facing" id="frontdoor_facing" class="selectpicker">
                                <option value="" disabled selected>Please select</option>
                                <option value="north">North</option>
                                <option value="south">South</option>
                                <option value="east">East</option>
                                <option value="west">West</option>
                            </select>
                        </div>
                        </div>
                       
                   <div class="col-12 form-group-inner brand-checks">
                            <input type="checkbox" name="office_rural" id="office_rural" onchange="use_outdoors_change(this)">
                            <label for="office_rural">Is the office in a rural area?</label>
                        </div>
                        <div class="col-12 form-group-inner brand-checks">
                            <input type="checkbox" name="radiography-used" id="radiography_used" onchange="use_outdoors_change(this)">
                            <label for="radiography_used">Will CBCT radiography be used?</label>
                        </div>
                    </div>

                    <div class="row gutter-10">
                        <div class="col-md-6 col-6 form-group">
                            <label for="display_make">Is the office a pediatric or orthodontic office?</label>
                            <select name="type_office" id="type_office" class="selectpicker">
                                <option value="" disabled selected>Please select</option>
                                <option value="pediatric">Pediatric</option>
                                <option value="orthodontic">Orthodontic</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-6 form-group">
                            <label for="display_model">How many people will be at the front desk?</label>
                            <div class="quantity">
                                <input type="number" name="front_desk_people" min="1" max="100" step="1" value="1">
                            </div>
                        </div>
                    </div>

                    <div class="row gutter-10">
                        <div class="col-md-6 col-6 form-group">
                            <label for="display_model">How many staff will be present at any one time?</label>
                            <div class="quantity">
                                <input type="number" name="num_of_staff" min="1" max="100" step="1" value="1">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /. display tab -->
                <div class="tab-pane fade" id="spec_tab" role="tabpanel" aria-labelledby="spec-tab">
                    <div class="table-responsive">
                        <table class="table table-list table-striped-darker tbl-lg" border="0">
                            <thead class="second-head">
                            <tr>
                                <th class="nowrap" style="padding-left:0;">SINGLE DISPLAY</th>
                                <th class="text-right" colspan="2" style="min-width: 117px;">Lorem Impsum</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="2" style="padding-left:0;">
                                    <div class="radio-reds">
                                        <div class="form-check">
                                            <input type="radio" id="display-metric" name="specifications_display" checked value="normal">
                                            <label for="display-metric">Normal</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" id="display-imperial" name="specifications_display" value="vip">
                                            <label for="display-imperial">VIP</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr><th class="nowrap">Display Weight</th>
                                <td></td>
                                <td align="right">11.5 (m)</td>
                            </tr>
                            <tr><th class="nowrap">Display Width</th>
                                <td></td>
                                <td align="right" class="nowrap">480 (m)</td>
                            </tr>
                            <tr><th class="nowrap">Display Height</th>
                                <td></td>
                                <td align="right" class="nowrap">480 (m)</td>
                            </tr>
                            <tr><th class="nowrap">Lorem</th>
                                <td></td>
                                <td align="right" class="nowrap">3</td>
                            </tr>
                            <tr><th class="nowrap">Lorem 2</th>
                                <td></td>
                                <td align="right" class="nowrap">35 (m)</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <p class="spacer-xs"></p>

                    <div class="table-responsive">
                        <table class="table table-list table-striped-darker tbl-lg">
                            <thead class="second-head">
                            <tr>
                                <th class="nowrap" style="padding-left:0;">Your Floorplan</th>
                                <th class="text-right" style="min-width: 117px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="2" style="padding-left:0;">
                                    <div class="radio-reds">
                                        <div class="form-check">
                                            <input type="radio" id="display-metric2" name="configuration_display" checked value="normal">
                                            <label for="display-metric2">Normal</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" id="display-imperial2" name="configuration_display" value="vip">
                                            <label for="display-imperial2">VIP</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr><th width="235px" style="min-width: 180px;">Number of Rooms</th>
                                <td align="right">4</td>
                            </tr>
                            <tr><th width="235px" style="min-width: 180px;">Display Weight</th>
                                <td align="right">2 (m)</td>
                            </tr>
                            <tr><th width="235px" style="min-width: 180px;">Display Width</th>
                                <td align="right" class="nowrap">1920 (m)</td>
                            </tr>
                            <tr><th width="235px" style="min-width: 180px;">Display Height</th>
                                <td align="right" class="nowrap">960 (m)</td>
                            </tr>
                            <tr><th width="235px" style="min-width: 180px;">Display Surface Area</th>
                                <td align="right" class="nowrap">1,843,200 (m)</td>
                            </tr>
                            <tr><th width="235px" style="min-width: 180px;">Lorem</th>
                                <td align="right" class="nowrap">6788.2 (m)</td>
                            </tr>
                            <tr><th width="235px" style="min-width: 180px;">Lorem 2</th>
                                <td align="right" class="nowrap">1600 (cm)</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /. specifications tab -->
            </div>

            <div class="bottom-controls">
                <button type="submit" class="button btn-red btn-wide">CREATE FLOORPLAN</button>
            </div>

        </form>
    </div>

    <div class="inner-pad pt-0">
        <hr>
        <a href="javascript:;" class="icon-link pt-2 d-block"><i class="fas fa-arrow-left"></i>Collapse menu</a>
    </div>
</div>


<header id="main-header" class="main-header">
    <nav id="navbar" class="has_fixed_navbar main-nav animate-me" style="top: 0px;">
        <div class="nav-logo">
            <a href="javascript:void(0)" id="nav-trigger"></a>
        </div>
        <div class="nav-center">
            <div id="nav-user" class="nav-user">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose an item
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>

            <div class="nav-title">
                <h2>Dr Tholen's Office</h2>
            </div>

            <!-- SIDEBAR TOGGLE -->
            <ul>
                <li class="notification dropdown">
                    <a href="javascript:void(0)" id="nav-notification-trigger" title="View your notifications" data-toggle="dropdown" aria-expanded="false">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning">4</span>
                    </a>
                </li>
            </ul>

        </div> <!-- /. nav-left -->

        <!-- EXTRA BUTTONS ABOVE THE SIDBAR -->
        <div class="nav-buttons">
            <!-- user info -->
            <div id="nav-user" class="nav-user">
                <img alt="" src="{{asset('assets/img/gravatar.jpg')}}" class="avatar avatar-96 photo" height="35" width="35">
            </div>
            <!-- /. user info -->
        </div>
        <div class="nav-buttons">
            <a class="nav-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    </div>
    </nav>
</header>


<!-- main-content -->
<section id="main-content" class="main-content">
<iframe
     src="https://codesandbox.io/embed/distracted-dawn-q60jb?fontsize=14&hidenavigation=1&theme=dark"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="distracted-dawn-q60jb"
     allow="geolocation; microphone; camera; midi; vr; accelerometer; gyroscope; payment; ambient-light-sensor; encrypted-media; usb"
     sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
   ></iframe>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
<script src="{{asset('assets/js/app.js')}}" ></script>
<script src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>

<script>
$(document).ready(function () {
    $('#rooms').on("change paste keyup", function() {
        $('#footage').val(($(this).val() * 500));
    });
    $('#footage').on("change paste keyup", function() {
        $footage_value = ($(this).val() / 500);
        $footage_value = ~~$footage_value;
        $('#rooms').val($footage_value);
    });
});
</script>
</body>
</html>
