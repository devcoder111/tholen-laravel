<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FloorLayers extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'layer',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                // width & height will always in the px unit.
                $table->string('name', 60);
                $table->string('type', 60);
                $table->float('x');
                $table->float('y');
                $table->float('width');
                $table->float('length');
                $table->string('bg_image');
                $table->string('bg_color', 10);
                $table->integer('layer_priority');
                $table->integer('parent_id');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(
            'layer'
        );
    }
}
