<?php

namespace App\Http\Controllers;

use App\Floor;
use Illuminate\Http\Request;

class FloorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        return view('welcome');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'operatory_rooms' => 'required',
                'square_footage'  => 'required',
                'building_shape'  => 'required',
            ]
        );
        $floor                    = new Floor();
        $floor->operatory_rooms   = $request->operatory_rooms;
        $floor->frontdoor_facing  = $request->frontdoor_facing;
        $floor->square_footage    = $request->square_footage;
        $floor->building_shape    = ($request->building_shape) ? $request->building_shape : 50;
        $floor->rural_area        = ($request->office_rural) == 'on' ? 1 : 0;
        $floor->cbct              = ($request->radiography_used) == 'on' ? 1 : 0;
        $floor->type              = $request->type_office;
        $floor->front_desk_people = $request->front_desk_people;
        $floor->number_of_staff   = $request->num_of_staff;
        $floor->display           = $request->specifications_display;
        $floor->floorplan         = $request->configuration_display;

        $percentage = ($request->building_shape) ? $request->building_shape : 50;
        $length     = ($percentage / 100) * $request->square_footage;
        $width      = ($request->square_footage - $length);

        $floor->length = $length;
        $floor->width  = $width;

        $floor->save();

        return redirect()->back()->withSuccess('floor added');
    }
}
